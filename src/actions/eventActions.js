import * as types from '../constants/actionTypes';

import axios from 'axios';
const EVENT_BRITE_OAUTH_TOKEN = '73XYX64DGGYEPBQZNBGW';
const EVENT_BRITE_LOCATION_WITHIN = '1000km';

function fetchEvents(){
	return {
		type : types.FETCH_EVENTS
	};
}

function receiveEvents(events){
	return {
		type : types.RECEIVE_EVENTS,
		events
	};
}

function trackEvents(id){
	return {
		type: types.TRACK_EVENTS,
		id
	};
}

export function addToTrackList(event){
	return function (dispatch) {
		localStorage.setItem(event.id,JSON.stringify(event));
		dispatch(trackEvents(event.id));
	}
}


export function getGeolocation() {
  return function (dispatch) {
    if (!navigator.geolocation) {
      return dispatch({
        type: RECEIVE_LOCATION,
        ...defaultPosition,
      });
    }
    return dispatch(requestGeolocation());
  }
}



export function getEvents(location) {
	const config = {
				params: {
					'token' : EVENT_BRITE_OAUTH_TOKEN,
					'sort_by' : 'date'
				}
			};

	if (typeof location != 'undefined'){
		config['params']['location.latitude'] = location.coords.latitude;
		config['params']['location.longitude'] = location.coords.latitude;
		config['params']['location.within'] = EVENT_BRITE_LOCATION_WITHIN;
	}
	else{
		config['params']['location.address'] = 'Singapore';
	}

	return function (dispatch) {
		dispatch(fetchEvents());

		return axios
				.get('https://www.eventbriteapi.com/v3/events/search/',config)
				.then(response => {
					if(response.status == 200){
						dispatch(receiveEvents(response.data.events));
					}
					return response;
				});			
	}
}