import React from 'react';
import PropTypes from 'prop-types';
import EventDate from './EventDate';

const EventItem = ({ event,onTrackEventClicked }) => (
	<li className='list-group-item'>
		<div className='row'>
			<div className='col-md-3'>

			</div>
			<div className='col-md-12'>	
				<h3>{event.name.text}</h3>
				<p>{ event.end.local }</p>
				<p>{event.description.text}</p>
			</div>
		</div>

		<button 
		className="btn btn-success"
		onClick={onTrackEventClicked}>
			Track
		</button>
  	</li>
)

export default EventItem