import React from 'react';
import PropTypes from 'prop-types'

const EventList = ({ children }) => (
  <div>
    <div>{children}</div>
  </div>
)

EventList.propTypes = {
  children: PropTypes.node
}

export default EventList