import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import EventList from '../components/EventList';
import EventItem from '../components/EventItem';
import { addToTrackList } from '../actions/eventActions'


const EventListContainer = ({  events,addToTrackList }) => (
	<EventList >
	<ul className="list-group">
	{events.map(event =>
		<EventItem
			key={event.id}
			event={event}
			onTrackEventClicked={() => addToTrackList(event)} />
	)}
	</ul>
	</EventList>
)

const mapStateToProps = (state) => {
	return {
		events: (typeof state.eventReducer.events == "undefined") ? [] : state.eventReducer.events
	}
}

export default connect(mapStateToProps,{addToTrackList})(EventListContainer)