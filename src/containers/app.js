import React, { Component } from 'react';
import EventListContainer from './EventListContainer';

class App extends Component {
	render(){
		return (
			<div>
			<h1>Event List</h1>
			<EventListContainer />
			</div>
		);
	}
}

export default App;