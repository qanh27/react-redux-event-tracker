import React from 'react';
import { render } from 'react-dom';
import thunkMiddleware from 'redux-thunk';
import { createStore, applyMiddleware  } from 'redux'
import { Provider } from 'react-redux'
import { createLogger } from 'redux-logger';
import rootReducer from './reducers';
import App from './containers/app';


import { getEvents } from './actions/eventActions'

const loggerMiddleware = createLogger()

const store = createStore(
	rootReducer,
	{'eventReducer':{'events':[]}},
	applyMiddleware(
		thunkMiddleware, 
		loggerMiddleware 
	)
)

//get Location
function getLocation() {
    if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
	} else {
		store.dispatch(getEvents());
	}
}
function geoSuccess(position) {
	store.dispatch(getEvents(position));
}
function geoError() {
	store.dispatch(getEvents());
}

getLocation();

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app')
);