import { combineReducers } from 'redux';
import * as types from '../constants/actionTypes';

function eventReducer(
	state = {
		isFetching : false,
		events : []
	},
	action
){
	switch (action.type) {
		case types.FETCH_EVENTS:
			return {
				...state,
				isFetching: true,
				events: []
			};

		case types.RECEIVE_EVENTS:
			return {
				...state,
				isFetching: false,
				events: action.events
			};

		default :
			return state;
	}
}

const rootReducer =  combineReducers({
	eventReducer
});

export default rootReducer